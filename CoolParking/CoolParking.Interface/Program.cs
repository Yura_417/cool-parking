﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.Interface.ConsoleIO;
using System;

namespace CoolParking.Interface
{
    class Program
    {
        static MainManager mainManager;
        static ParkingService parkingService;
        static TimerService timerService;
        static TimerService timer;
        static ILogService logService;

        static void Main(string[] args)
        {
            Settings.SetConsoleParam();
            Console.Title = "CoolParking";

            timerService = new TimerService();
            timer = new TimerService();

            parkingService = new ParkingService(timerService, timer, logService);
            mainManager = new MainManager(parkingService);
            mainManager.Run();
        }
    }
}
