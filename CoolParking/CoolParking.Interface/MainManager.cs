﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.Interface.ConsoleIO;
using CoolParking.Interface.ConsoleUI;
using System;

namespace CoolParking.Interface
{
    public class MainManager : CommandManager
    {
        protected override void IniCommandsInfo()
        {
            commandsInfo = new CommandInfo[]
            {
                new CommandInfo("Завершити роботу", null),
                new CommandInfo("Поточний баланс Паркінгу",CurrentBalanceOfParking),
                new CommandInfo("Сума зароблених коштів за поточний період",TheAmountOfMoneyEarned),
                new CommandInfo("Кількість вільних місць на паркуванні",NumberOfVacancies),
                new CommandInfo("Транзакції Паркінгу за поточний період",ParkingTransactions),
                new CommandInfo("Історія Транзакцій",TransactionHistory),
                new CommandInfo("Список Тр. засобів на Паркінгу", ListOfVehicles),
                new CommandInfo("Поставити Тр. засіб на Паркінг", PutTheVehicle),
                new CommandInfo("Забрати Тр. засіб з Паркінгу", PickUpTheVehicle),
                new CommandInfo("Поповнити баланс Тр. засобу", TopUpTheBalanceOfTheVehicle),
            };
        }

        private ParkingService _parkingService;


        public MainManager(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        private void CurrentBalanceOfParking()
        {
            Console.WriteLine($"Поточний баланс Паркінгу: {CoolParking.BL.Models.Settings.initialBalance}");
            RequestForContinuation();
        }

        private void TheAmountOfMoneyEarned()
        {
            Console.WriteLine($"Сума зароблених коштів за поточний період: {_parkingService.GetBalance()}");
            RequestForContinuation();
        }

        private void NumberOfVacancies()
        {
            Console.WriteLine($"Вільних місць: {_parkingService.GetFreePlaces()}/{_parkingService.GetCapacity()}");
            RequestForContinuation();
        }

        private void ParkingTransactions()
        {
            foreach(var obj in _parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine($"Зняття у {obj.vehicle.Id}         час: {obj.Time}   залишок: {obj.Sum}");
            }
            RequestForContinuation();
        }

        private void TransactionHistory()
        {
            Console.WriteLine(_parkingService.ReadFromLog());
            RequestForContinuation();
        }

        private void ListOfVehicles()
        {
            foreach (var obj in _parkingService.GetVehicles())
            {
                Console.WriteLine($"{obj.Id,7} {obj.VehicleType,-15} {(obj.Balance ),-20}");
            }
            RequestForContinuation();
        }

        private void PutTheVehicle()
        {
            if(_parkingService.GetFreePlaces() == 0)
            {
                Console.WriteLine("Відсутні вільні місця на парковці!!!");
                RequestForContinuation();
            }
            else
            {
                Vehicle inst = new Vehicle();
                inst.Id = "";
                int type = Entering.EnterInt32("Оберіть тип транспорту(1.PassengerCar, 2.Truck, 3.Bus, 4.Motorcycle)", 1, 4);
                switch (type)
                {
                    case 1:
                        inst.VehicleType = VehicleType.PassengerCar;
                        break;
                    case 2:
                        inst.VehicleType = VehicleType.Truck;
                        break;
                    case 3:
                        inst.VehicleType = VehicleType.Bus;
                        break;
                    case 4:
                        inst.VehicleType = VehicleType.Motorcycle;
                        break;
                }
                inst.Balance = Entering.EnterInt32("Balanse");
                _parkingService.AddVehicle(inst);
            }
        }

        private void PickUpTheVehicle()
        {
            string vehicleId = Entering.EnterString("Id");
            _parkingService.RemoveVehicle(vehicleId);
        }

        private void TopUpTheBalanceOfTheVehicle()
        {
            string vehicleId = Entering.EnterString("Id");
            int sum = Entering.EnterInt32("Sum");
            _parkingService.TopUpVehicle(vehicleId, sum);
        }

        protected override void PrepareScreen()
        {
            Console.Clear();
            Console.WriteLine("---Інформація про Паркінг---");
        }
    }
}
