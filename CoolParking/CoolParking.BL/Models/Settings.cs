﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal initialBalance { get; set; } = 0;
        public static int Capacity { get; set; } = 10;
        public static int writeOff = 5000;
        public static int recordingPeriod = 60000;
        public static double TariffPassengerCar = 2;
        public static double TariffTruck = 5;
        public static double TariffBus = 3.5;
        public static double TariffMotorcycle = 1;
        public static double coefficient = 2.5;
    }
}
