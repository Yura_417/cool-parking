﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal ParkingBalanse { get; set; }
        public int Capasity { get; set; }
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
    }
}
