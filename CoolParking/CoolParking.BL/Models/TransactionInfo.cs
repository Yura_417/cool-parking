﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }
        public Vehicle vehicle;

        //public Vehicle Vehicle
        //{
        //    get { return vehicle; }
        //    set
        //    {
        //        if (value == null)
        //        {
        //            throw new ArgumentNullException("Vehicle");
        //        }
        //        vehicle = value;
        //    }
        //}

        public TransactionInfo(decimal sum, DateTime time, Vehicle vehicle)
        {
            Sum = sum;
            Time = time;
            this.vehicle = vehicle;
        }

    }
}
