﻿using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;


        public string Id
        {
            get { return id; }
            set
            {
                //Regex
                id = value;
            }
        }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        public Vehicle(string _id, VehicleType type, decimal _balance)
        {
            Id = _id;
            VehicleType = type;
            Balance = _balance;
        }

        public Vehicle() { }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] letters = "ABSDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            Random random = new Random();
            string registrationNumber = string.Empty;
            int valueRandom;

            valueRandom = random.Next(0, letters.Length);
            registrationNumber = letters[valueRandom].ToString();
            valueRandom = random.Next(0, letters.Length);
            registrationNumber += letters[valueRandom].ToString() + "-";

            valueRandom = random.Next(1000, 10000);
            registrationNumber += valueRandom.ToString() + "-";

            valueRandom = random.Next(0, letters.Length);
            registrationNumber += letters[valueRandom].ToString();
            valueRandom = random.Next(0, letters.Length);
            registrationNumber += letters[valueRandom].ToString();

            return registrationNumber;
        }
    }
}
