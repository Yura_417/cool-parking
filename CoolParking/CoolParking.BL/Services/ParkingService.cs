﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly TimerService _withdrawTimer;
        readonly TimerService _logTimer;
        readonly ILogService _logService;
        public static LogService logService = new LogService();

        public static Parking Parking = new Parking();

        public ParkingService(TimerService withdrawTimer, TimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _withdrawTimer.Start();
        }

        public ParkingService() { }

        public void AddVehicle(Vehicle vehicle)
        {
            vehicle.Id = Vehicle.GenerateRandomRegistrationPlateNumber();
            Parking.Vehicles.Add(vehicle);

    }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.ParkingBalanse;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - Parking.Vehicles.Count();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo[] transactionInfo = TimerService.listTransaction.ToArray();
            return transactionInfo;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var vehicle = new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
            return vehicle;
        }

        public string ReadFromLog()
        {
            LogService log = new LogService();
            return log.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Parking.Vehicles.Remove(Parking.Vehicles.FirstOrDefault(e => e.Id == vehicleId));
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = Parking.Vehicles.FirstOrDefault(e => e.Id == vehicleId);
            vehicle.Balance += sum;
            logService.Write($"Поповнення коштів {vehicle.Id} час:{DateTime.Now} сума:{sum} баланс:{vehicle.Balance}");
        }
    }
}
