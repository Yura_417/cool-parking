﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _Timer;
        private Timer _TimerLog;
        private double interval = Settings.writeOff;
        private double logInterval = Settings.recordingPeriod;
        public static Parking parking =  ParkingService.Parking;
        public static LogService logService = new LogService();
        public static List<TransactionInfo> listTransaction = new List<TransactionInfo>();
        private static TransactionInfo transactions;

        public double Interval { get { return interval; } set { interval = value; } }

        public event ElapsedEventHandler Elapsed;

        public void FireElapsedEvent()
        {
            _Timer.Elapsed += OnTimedEvent;
            _TimerLog.Elapsed += OnLogEvent;
            Elapsed?.Invoke(this, null);
        }

        public TimerService()
        {
            _Timer = new Timer();
            _Timer.Elapsed += OnTimedEvent; 
            _Timer.Enabled = false;
            _Timer.Interval = Interval;

            _TimerLog = new Timer();
            _TimerLog.Elapsed += OnLogEvent;
            _TimerLog.Enabled = false;
            _TimerLog.Interval = logInterval;

            parking.ParkingBalanse = 0;
            listTransaction.Clear();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            foreach (var obj in parking.Vehicles)
            {
                //PassengerCar
                if(obj.VehicleType == VehicleType.PassengerCar)
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (decimal)(Settings.TariffPassengerCar * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - (decimal)Settings.TariffPassengerCar) < 0)
                    {
                        decimal fine = obj.Balance + ((decimal)((decimal)Settings.TariffPassengerCar - obj.Balance) * (decimal)Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= (decimal)Settings.TariffPassengerCar;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += (decimal)Settings.TariffPassengerCar;
                        Settings.initialBalance += (decimal)Settings.TariffPassengerCar;
                    }
                }
                //Truck
                else if(obj.VehicleType == VehicleType.Truck)
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (decimal)(Settings.TariffTruck * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - (decimal)Settings.TariffTruck) < 0)
                    {
                        decimal fine = obj.Balance + ((decimal)((decimal)Settings.TariffTruck - obj.Balance) * (decimal)Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= (decimal)Settings.TariffTruck;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += (decimal)Settings.TariffTruck;
                        Settings.initialBalance += (decimal)Settings.TariffTruck;
                    }
                }
                //Bus
                else if (obj.VehicleType == VehicleType.Bus)
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (decimal)(Settings.TariffBus * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - (decimal)Settings.TariffBus) < 0)
                    {
                        decimal fine = obj.Balance + ((decimal)((decimal)Settings.TariffBus - obj.Balance) * (decimal)Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= (decimal)Settings.TariffBus;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += (decimal)Settings.TariffBus;
                        Settings.initialBalance += (decimal)Settings.TariffBus;
                    }
                }
                //Motorcycle
                else if (obj.VehicleType == VehicleType.Motorcycle)
                {
                    if (obj.Balance <= 0)
                    {
                        decimal fine = (decimal)(Settings.TariffMotorcycle * Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else if (obj.Balance > 0 && (obj.Balance - (decimal)Settings.TariffMotorcycle) < 0)
                    {
                        decimal fine = obj.Balance + ((decimal)((decimal)Settings.TariffMotorcycle - obj.Balance) * (decimal)Settings.coefficient);
                        obj.Balance -= fine;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += fine;
                        Settings.initialBalance += fine;
                    }
                    else
                    {
                        obj.Balance -= (decimal)Settings.TariffMotorcycle;
                        transactions = new TransactionInfo(obj.Balance, DateTime.Now, obj);
                        listTransaction.Add(transactions);
                        parking.ParkingBalanse += (decimal)Settings.TariffMotorcycle;
                        Settings.initialBalance += (decimal)Settings.TariffMotorcycle;
                    }
                }

            }
        }

        private static void OnLogEvent(Object source, ElapsedEventArgs e)
        {
            foreach (var obj in listTransaction.ToArray())
            {
                logService.Write($"Зняття у {obj.vehicle.Id}         час: {obj.Time}   залишок: {obj.Sum}");
            }
            listTransaction.Clear();
            parking.ParkingBalanse = 0;
        }

        public void Dispose()
        {
            _Timer.Dispose();
            _TimerLog.Dispose();
        }

        public void Start()
        {
            _Timer.Enabled = true;
            _TimerLog.Enabled = true;
        }

        public void Stop()
        {
            _Timer.Enabled = false;
            _TimerLog.Enabled = false;
        }
    }
}
